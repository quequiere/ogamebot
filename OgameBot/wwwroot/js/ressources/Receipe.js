(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./RessourceEnum.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const RessourceEnum_js_1 = require("./RessourceEnum.js");
    class Receipe {
        constructor(ressources) {
            this.ressourceMap = ressources;
        }
        static generateReceipeFromWeb(costs) {
            var builder = new ReceipeBuilder();
            for (var x = 0; x < costs.children().length; x++) {
                var child = costs.children()[x];
                var typeName = child.classList[0];
                var type = RessourceEnum_js_1.RessourceEnum.stringToRessource(typeName);
                var amount = Number(child.innerText.trim().replace(".", ""));
                builder.setRessource(type, amount);
            }
            return builder.build();
        }
    }
    exports.Receipe = Receipe;
    class ReceipeBuilder {
        constructor() {
            this.ressourceMap = new Map();
            this.ressourceMap.set(RessourceEnum_js_1.RessourceEnum.metal, 0);
            this.ressourceMap.set(RessourceEnum_js_1.RessourceEnum.crystal, 0);
            this.ressourceMap.set(RessourceEnum_js_1.RessourceEnum.deuterium, 0);
        }
        setRessource(ressource, amount) {
            this.ressourceMap.delete(ressource);
            this.ressourceMap.set(ressource, amount);
        }
        build() {
            return new Receipe(this.ressourceMap);
        }
    }
});
