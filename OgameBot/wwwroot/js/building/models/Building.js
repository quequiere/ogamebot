(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../../ressources/Receipe.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const Receipe_js_1 = require("../../ressources/Receipe.js");
    class Building {
        constructor(id, name) {
            this.id = id;
            this.name = name;
        }
        getConstructionCost() {
            var request = $.ajax({
                type: "GET",
                url: "https://s165-fr.ogame.gameforge.com/game/index.php?page=" + this.getBuildType() + "&ajax=1&type=" + this.id,
                async: false
            }).responseText;
            var requestHtml = $.parseHTML(request);
            var find = $(requestHtml).find("#costs");
            return Receipe_js_1.Receipe.generateReceipeFromWeb(find);
        }
        expectedUrlToBuild() {
            return "?page=" + this.getBuildType();
        }
    }
    exports.Building = Building;
});
