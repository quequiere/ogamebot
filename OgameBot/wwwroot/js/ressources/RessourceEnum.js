(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var RessourceEnum;
    (function (RessourceEnum) {
        RessourceEnum["metal"] = "metal";
        RessourceEnum["crystal"] = "crystal";
        RessourceEnum["deuterium"] = "deuterium";
    })(RessourceEnum = exports.RessourceEnum || (exports.RessourceEnum = {}));
    (function (RessourceEnum) {
        function stringToRessource(ressourceName) {
            if (ressourceName == "metal")
                return RessourceEnum.metal;
            else if (ressourceName == "crystal")
                return RessourceEnum.crystal;
            else if (ressourceName == "deuterium")
                return RessourceEnum.deuterium;
            throw new Error("Failed to parse ressource name: " + ressourceName);
        }
        RessourceEnum.stringToRessource = stringToRessource;
    })(RessourceEnum = exports.RessourceEnum || (exports.RessourceEnum = {}));
});
