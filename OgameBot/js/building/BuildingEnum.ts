﻿import { Building } from "./models/Building.js";
import { RessourceBuilding } from "./models/RessourceBuidling.js";


export enum BuildingEnum {
    metalMine = "metalMine",
    crystallMine = "crystallMine",
    deutheriumSynthetisator = "deutheriumSynthetisator",
    sunlightCentral = "sunlightCentral"
}


var buildings = new Map();
buildings.set("metalMine", new RessourceBuilding(1, "Metal Mine"));
buildings.set("crystallMine", new RessourceBuilding(2, "Crystall Mine"));
buildings.set("deutheriumSynthetisator", new RessourceBuilding(3, "Deutherium synthetisator"));
buildings.set("sunlightCentral", new RessourceBuilding(4, "Sunlight Central"));


export namespace BuildingEnum {
    export function toBuilding(buildingEnum: BuildingEnum): Building {

        var building = buildings.get(buildingEnum);
        if (!building) {
            throw new Error("Error while trying to get building " + buildingEnum);
        }

        return building;
    }
}

