﻿import { UserData } from "./UserData.js"
import { BotConfiguration } from "./BotConfiguration.js";
import { BuildingController } from "./building/BuildingController.js";


export class OgameBot
{
    static object: OgameBot;

    displayTooltip() {
        var toolTip = `
        <div> 
           OmegaBot started <br>
            Version: ${BotConfiguration.version}
        </div>`

        $("#toolbarcomponent").append(toolTip)
    }

    update() {
        console.log("Updating bot !");
        BuildingController.object.update();
    }
}

function startBot() {
    console.log("Ogame bot successfuly started !");

    OgameBot.object = new OgameBot();
    UserData.object = new UserData();
    BuildingController.object = new BuildingController();


    OgameBot.object.displayTooltip();
    setInterval(() => { OgameBot.object.update() }, BotConfiguration.waitTime)
}

setTimeout(() => { startBot() }, 2000);
