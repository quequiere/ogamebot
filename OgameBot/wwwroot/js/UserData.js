(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./ressources/RessourceEnum.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const RessourceEnum_js_1 = require("./ressources/RessourceEnum.js");
    class UserData {
        constructor() {
            console.log("Loading user data");
            this.printInfo();
        }
        getRessource(ressource) {
            var tagName = null;
            switch (ressource) {
                case RessourceEnum_js_1.RessourceEnum.metal:
                    tagName = "#resources_metal";
                    break;
                case RessourceEnum_js_1.RessourceEnum.crystal:
                    tagName = "#resources_crystal";
                    break;
                case RessourceEnum_js_1.RessourceEnum.deuterium:
                    tagName = "#resources_deuterium";
                    break;
            }
            var a = $(tagName).text();
            return Number(a.replace(".", ""));
        }
        getDarkMatter() {
            var a = $("#resources_darkmatter").text();
            return Number(a.replace(".", ""));
        }
        getEnergy() {
            var a = $("#resources_energy").text();
            return Number(a.replace(".", ""));
        }
        hasPositiveEnergy() {
            if (this.getEnergy() > 0)
                return true;
            return false;
        }
        printInfo() {
            console.log("======== User data information =======");
            console.log(`Metal: ${this.getRessource(RessourceEnum_js_1.RessourceEnum.metal)}`);
            console.log(`Crystals: ${this.getRessource(RessourceEnum_js_1.RessourceEnum.crystal)}`);
            console.log(`Deuterium: ${this.getRessource(RessourceEnum_js_1.RessourceEnum.deuterium)}`);
            console.log(`DarkMatter: ${this.getDarkMatter()}`);
            console.log(`Energy: ${this.getEnergy()}`);
        }
    }
    exports.UserData = UserData;
});
