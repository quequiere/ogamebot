﻿import { Building } from "./Building.js";

export class RessourceBuilding extends Building{

    constructor(id: number, name: string) {
        super(id,name);
    }

    getBuildType(): string {
        return "resources";
    }

}