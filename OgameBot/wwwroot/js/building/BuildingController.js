(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./BuildingEnum.js", "./BuildingData.js", "../UserData.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const BuildingEnum_js_1 = require("./BuildingEnum.js");
    const BuildingData_js_1 = require("./BuildingData.js");
    const UserData_js_1 = require("../UserData.js");
    class BuildingController {
        update() {
            if (!this.isBuilding()) {
                if (!UserData_js_1.UserData.object.hasPositiveEnergy()) {
                    console.log("Need more power, build solar pannel");
                    this.startBuild(BuildingEnum_js_1.BuildingEnum.sunlightCentral);
                }
            }
            else {
                console.log("Is building ... ");
            }
        }
        isBuilding() {
            return $(".construction.active:first tr").length > 1 ? true : false;
        }
        checkGoodUrl(building) {
            var currentLocation = window.location.search;
            if (currentLocation != building.expectedUrlToBuild()) {
                console.log("Changing url to build: " + building.name);
                window.location.search = building.expectedUrlToBuild();
            }
        }
        getConstructorToken() {
            var commandLine = $(".fastBuild:first").attr("onclick");
            var regex = new RegExp('token=([^.]+)\'', 'g');
            var exe = regex.exec(commandLine);
            return exe[1];
        }
        sendBuildRequest(build) {
            console.log("Send build request !");
            var url = '?page=' + build.getBuildType() + '&modus=1&type=' + build.id + '&menge=1&token=' + this.getConstructorToken();
            window.location.search = url;
        }
        startBuild(building) {
            var build = BuildingEnum_js_1.BuildingEnum.toBuilding(building);
            if (!BuildingData_js_1.BuildingData.hasEnoughRessources(building)) {
                console.log("Failed to build cause not egought ressources to build" + building.toString());
            }
            this.checkGoodUrl(build);
            this.sendBuildRequest(build);
        }
    }
    exports.BuildingController = BuildingController;
});
