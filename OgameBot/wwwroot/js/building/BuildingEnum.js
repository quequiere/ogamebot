(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./models/RessourceBuidling.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const RessourceBuidling_js_1 = require("./models/RessourceBuidling.js");
    var BuildingEnum;
    (function (BuildingEnum) {
        BuildingEnum["metalMine"] = "metalMine";
        BuildingEnum["crystallMine"] = "crystallMine";
        BuildingEnum["deutheriumSynthetisator"] = "deutheriumSynthetisator";
        BuildingEnum["sunlightCentral"] = "sunlightCentral";
    })(BuildingEnum = exports.BuildingEnum || (exports.BuildingEnum = {}));
    var buildings = new Map();
    buildings.set("metalMine", new RessourceBuidling_js_1.RessourceBuilding(1, "Metal Mine"));
    buildings.set("crystallMine", new RessourceBuidling_js_1.RessourceBuilding(2, "Crystall Mine"));
    buildings.set("deutheriumSynthetisator", new RessourceBuidling_js_1.RessourceBuilding(3, "Deutherium synthetisator"));
    buildings.set("sunlightCentral", new RessourceBuidling_js_1.RessourceBuilding(4, "Sunlight Central"));
    (function (BuildingEnum) {
        function toBuilding(buildingEnum) {
            var building = buildings.get(buildingEnum);
            if (!building) {
                throw new Error("Error while trying to get building " + buildingEnum);
            }
            return building;
        }
        BuildingEnum.toBuilding = toBuilding;
    })(BuildingEnum = exports.BuildingEnum || (exports.BuildingEnum = {}));
});
