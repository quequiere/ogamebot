(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./Building.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const Building_js_1 = require("./Building.js");
    class RessourceBuilding extends Building_js_1.Building {
        constructor(id, name) {
            super(id, name);
        }
        getBuildType() {
            return "resources";
        }
    }
    exports.RessourceBuilding = RessourceBuilding;
});
